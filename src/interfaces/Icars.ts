export interface ICar {
  model: string;
  year: number;
  speed: number;
  toHundred: number;
  engine: string;
  cubic: number;
  horsepower: number;
  price: number;
  description: string
}

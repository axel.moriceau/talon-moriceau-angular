export interface IRandomUser {
  name: {
      title: string,
      first: string,
      last: string
    },
    picture: {
      last: string,
      large: string,
      thumbnail: string
    }
}

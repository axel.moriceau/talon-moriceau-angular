import { SliderComponent } from './slider/slider.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CarsComponent } from './pages/cars/cars.component';
import { FerrariComponent } from './pages/ferrari/ferrari.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from './error404/error404.component';

const routes: Routes = [
  { path: '', redirectTo: '/cars', pathMatch: 'full' },
  { path: 'cars', component: SliderComponent },
  { path: 'ferrari', component: FerrariComponent },
  { path: 'cars/:id', component: CarsComponent },
  { path: 'contact', component: ContactComponent },
  {path: '404', component: Error404Component},
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

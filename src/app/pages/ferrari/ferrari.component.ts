import {
  IRandomUser
} from './../../../interfaces/IRandomUser';
import {
  RandomUserService
} from './../../services/random-user.service';
import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'app-ferrari',
  templateUrl: './ferrari.component.html',
  styleUrls: ['./ferrari.component.scss']
})
export class FerrariComponent implements OnInit {
  logo_url = '../../assets/images/logo/logo_ferrari.png'
  about_us_url = '../../assets/images/about_us/about_us.jpg'
  history_url = '../../assets/images/about_us/history.png'

  constructor(private userService: RandomUserService) {}

  users: IRandomUser[];

  ngOnInit(): void {
    this.getUsers();
    console.log(this.users);
    document.title = 'About Us';
  }

  getUsers(): void {
    this.userService.getUser().subscribe(data => {
      // @ts-ignore
      this.users = data.results;
    });
  }
}

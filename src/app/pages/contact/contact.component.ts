import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  constructor() {}
  contact: {
    country: string;
    phone: string;
    mail: string;
    location: string;
    location_url: string;
  } = {
    country: 'IT',
    phone: '0536949111',
    mail: 'customerservice@owners.ferrari.com',
    location: 'Via Abetone Inferiore n. 4, I-41053 Maranello (MO)',
    location_url:
      'https://www.google.fr/maps/place/Motorsport+Maranello/@44.5303093,10.8584958,18.69z/data=!4m13!1m7!3m6!1s0x477fe3a68615c257:0x3c16ac6b4ee57f8a!2s41053+Maranello,+Mod%C3%A8ne,+Italie!3b1!8m2!3d44.5263024!4d10.8666834!3m4!1s0x477fe3a7ccccea57:0x547396e754faf7c0!8m2!3d44.530384!4d10.8589701',
  };
  supportedCountries = [
    { name: 'France', code: 'FR' },
    { name: 'U.S.A', code: 'US' },
    { name: 'Italia', code: 'IT' },
  ];
  contact_object = ['Appointment', 'Information', 'Work'];
  form_info = {
    first_name: '',
    last_name: '',
    phone: '',
    mail:'',
    region: this.supportedCountries[0].code,
    object: this.contact_object[0],
    description: '',
    descriptionURI: ''
  };
  ngOnInit(): void {
    const inputs = [
      'first_name',
      'last_name',
      'phone',
      'region',
      'mail',
      'object',
      'description',
    ];
    inputs.forEach((id) => {
      document
        .querySelector('#' + id)
        .addEventListener('input', (e) => {
          //@ts-ignore
          this.form_info[id] = e.target.value;
          if (id == 'description') {
            this.form_info.descriptionURI = encodeURI(this.form_info.description)
            console.log(this.form_info.descriptionURI);

          }
        });
    });
    document.title = 'Contact';
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarsService } from 'src/app/services/cars.service';
import { ICar } from 'src/interfaces/Icars';
import data from '../../../assets/data/cars';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss'],
})
export class CarsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private carsService: CarsService
  ) {}
  idx = 0;
  id: number;
  private sub: any;
  car: { id: number; img: string; imgAlt: string };
  carDetail: ICar;
  carsList: Array<{ id: number; img: string; imgAlt: string }>;
  ngOnInit(): void {
    this.sub = this.route.params.subscribe((params) => {
      this.idx = +params['id'];
    });
    this.getCarsList();
    if (this.idx >= this.carsList.length || this.idx < 0) {
      this.router.navigate(['/cars']);
    } else {
      document.querySelector('body').setAttribute('style', 'overflow:hidden;');
      this.car = this.carsList[this.idx];
      this.carDetail = data[this.idx];
      setTimeout(() => {
        document.querySelector('body').removeAttribute('style');
        document.querySelector('html').scrollTo(0, window.innerHeight);
      }, 3000);

      this.counter('speed', 0, this.carDetail.speed);
      this.counterDec('hundred', 20, this.carDetail.toHundred);
      this.counter('cubic', this.carDetail.cubic - 500, this.carDetail.cubic);
    }
  }
  counter(
    id: string,
    start: number,
    end: number,
    dec: boolean = false,
    duration: number = 4500
  ) {
    if (start === end) return;
    var range = end - start;
    var current = start;
    var increment = end > start ? 1 : -1;
    var stepTime = Math.abs(Math.floor(duration / range));
    var obj = document.getElementById(id);
    var timer = setInterval(function () {
      current += increment;
      obj.innerHTML = current.toString();
      if (current == end) {
        clearInterval(timer);
      }
    }, stepTime);
  }

  counterDec(id: string, start: number, end: number, duration: number = 5000) {
    end *= 10;
    start *= 10;
    if (start === end) return;
    var range = end - start;
    var current = start;
    var increment = end > start ? 1 : -1;
    var stepTime = Math.abs(Math.floor(duration / range));
    var obj = document.getElementById(id);
    var timer = setInterval(function () {
      current += increment;
      obj.innerHTML = (current / 10).toString();
      if (current / 10 == end / 10) {
        clearInterval(timer);
      }
    }, stepTime);
  }
  getCarsList(): void {
    this.carsList = [...this.carsService.getCarsList()];
  }
}

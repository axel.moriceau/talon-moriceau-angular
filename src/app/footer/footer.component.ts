import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  logo_url= '../../assets/images/logo/logo_ferrari.png'
  nav_items = [
    {
      title: 'Ferrari',
      link: 'ferrari',
    },
    {
      title: 'Our Cars',
      link: 'cars',
    },
    {
      title: 'Contact',
      link: 'contact',
    },
  ];
  authors=["TALON Romain", "MORICEAU Axel"]
  constructor() {}

  ngOnInit(): void {}
}

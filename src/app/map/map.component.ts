import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  map: mapboxgl.Map;
  marker: mapboxgl.Marker;
  style = 'mapbox://styles/mapbox/navigation-preview-night-v4';
  lat = 44.5326498;
  lng = 10.8623028;
  pin: HTMLElement = document.createElement('img');
  
  constructor() { }
  ngOnInit() {
    this.pin.setAttribute('src', '../../assets/images/logo/ecussonLight.svg')
    this.pin.classList.add('w-12');

    mapboxgl.accessToken = environment.mapbox.accessToken;
      this.map = new mapboxgl.Map({
        container: 'map',
        style: this.style,
        zoom: 15,
        center: [this.lng, this.lat]
    });
    // Add map controls
    this.map.addControl(new mapboxgl.NavigationControl());
    this.marker = new mapboxgl.Marker({element: this.pin})
    .setLngLat([10.8623028,44.5326498])
    .addTo(this.map);
  }
}
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { SliderComponent } from './slider/slider.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { MenuFullPageComponent } from './menu-full-page/menu-full-page.component';
import { FerrariComponent } from './pages/ferrari/ferrari.component';
import { CarsComponent } from './pages/cars/cars.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CarDescriptionComponent } from './car-description/car-description.component';
import { HttpClientModule } from '@angular/common/http';
import {PhonePipe} from '../pipes/phone.pipe';
import { MapComponent } from './map/map.component';
import { Error404Component } from './error404/error404.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    SliderComponent,
    SideMenuComponent,
    MenuFullPageComponent,
    FerrariComponent,
    CarsComponent,
    ContactComponent,
    CarDescriptionComponent,
    PhonePipe,
    MapComponent,
    Error404Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

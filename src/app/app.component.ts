import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'talon-moriceau-angular';
  logo_url= '../assets/images/logo/logo_ferrari.png'
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuFullPageComponent } from './menu-full-page.component';

describe('MenuFullPageComponent', () => {
  let component: MenuFullPageComponent;
  let fixture: ComponentFixture<MenuFullPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuFullPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuFullPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

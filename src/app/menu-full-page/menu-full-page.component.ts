import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-full-page',
  templateUrl: './menu-full-page.component.html',
  styleUrls: ['./menu-full-page.component.scss']
})
export class MenuFullPageComponent implements OnInit {
  logo_url= '../../assets/images/logo/logo_ferrari.png'

  nav_items = [
    {
      title: 'Ferrari',
      link: 'ferrari',
    },
    {
      title: 'Our Cars',
      link: 'cars',
    },
    {
      title: 'Contact',
      link: 'contact',
    },
  ];

  constructor() { }

  ngOnInit(): void { }

  removeBodyAttr() {
    document.querySelector('body').removeAttribute('style')
  }

}

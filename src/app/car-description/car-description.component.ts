import { CarsService } from './../services/cars.service';
import { Component, Input, OnInit } from '@angular/core';
import {ICar} from '../../interfaces/Icars'
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-car-description',
  templateUrl: './car-description.component.html',
  styleUrls: ['./car-description.component.scss'],
})
export class CarDescriptionComponent implements OnInit {
  @Input() carIdx: number;
  @Input() carUrl: string;
  @Input() slider: boolean
  car: ICar;
  carTab: ICar[];
  constructor(
    private carsService: CarsService,
    private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.getCars();
    this.car = this.carTab[this.carIdx];
    if (!this.slider) {
      document.title = this.car.model;
    }
  }
  getCars(): void {
    this.carTab = [...this.carsService.getCars()];
  }
}

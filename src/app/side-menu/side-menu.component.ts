import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  constructor() { }

  showMenu: boolean = false;
  ngOnInit(): void {
  }
  toggleMenu() {
    this.showMenu = !this.showMenu;
    if (this.showMenu) {
      document.querySelector('body').setAttribute('style','overflow:hidden')
    } else {
      document.querySelector('body').removeAttribute('style')
    }
  }
}

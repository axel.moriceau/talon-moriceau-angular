import { ICar } from 'src/interfaces/Icars';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import data from '../../assets/data/cars';

@Injectable({
  providedIn: 'root',
})
export class CarsService {
  constructor() {}

  getCars(): ICar[] {
    return data;
  }
  getCarsList(): Array<{id:number,img:string,imgAlt:string}> {
    return [
      {
        id: 0,
        img: '../../assets/images/cars/laferrari_aperta_1.jpg',
        imgAlt: '../../assets/images/cars/laferrari_aperta_2.jpg',
      },
      {
        id: 1,
        img: '../../assets/images/cars/f8_spider_1.jpg',
        imgAlt: '../../assets/images/cars/f8_spider_2.jpg',
      },
      {
        id: 2,
        img: '../../assets/images/cars/portofino_1.jpg',
        imgAlt: '../../assets/images/cars/portofino_2.jpg',
      },
      {
        id: 3,
        img: '../../assets/images/cars/sf90_stradale_1.jpg',
        imgAlt: '../../assets/images/cars/sf90_stradale_2.jpg',
      },
    ];
  }
}

import {
  IRandomUser
} from './../../interfaces/IRandomUser';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RandomUserService {

  headers = new HttpHeaders({
    'Access-Control-Allow-Origin': '*'
  });
  private readonly URL = 'https://randomuser.me/api/?results=8';

  constructor(private http: HttpClient) {}

  getUser(): Observable < IRandomUser[] > {
      return this.http.get<IRandomUser[]>(this.URL,{
        ...this.headers
     });
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.scss']
})
export class Error404Component implements OnInit {
  carUrl: string = '../../assets/images/cars/404.jpeg'

  constructor() { }
  ngOnInit(): void {
    document.title = 'ERROR 404';
  }

}

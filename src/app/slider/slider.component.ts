import { Component, OnInit } from '@angular/core';
import { CarsService } from '../services/cars.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent implements OnInit {
  constructor(private carsService: CarsService) {}
  startIndex = 1;
  carsList: Array<{ id: number; img: string; imgAlt: string }>;
  ngOnInit() {
    this.getCarsList()
    this.Repeat();
    document.title = 'Our Cars';
  }

  Repeat() {
    setTimeout(() => {
      this.__FunctionSlide();
      this.Repeat();
    }, 5000);
  }

  __FunctionSlide() {
    const slides = Array.from(document.getElementsByClassName('show-slide'));
    if (slides === []) {
      this.Repeat();
    }
    for (const x of slides) {
      const y = x as HTMLElement;
      y.style.display = 'none';
    }
    if (this.startIndex > slides.length - 1) {
      this.startIndex = 0;
      const slide = slides[this.startIndex] as HTMLElement;
      slide.style.display = 'block';
      this.startIndex++;
    } else {
      const slide = slides[this.startIndex] as HTMLElement;
      slide.style.display = 'block';
      this.startIndex++;
    }
  }
  getCarsList(): void {
    this.carsList = [...this.carsService.getCarsList()];
  }
}

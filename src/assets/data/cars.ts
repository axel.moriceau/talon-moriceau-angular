const data: Array<{
  model: string;
  year: number;
  speed: number;
  toHundred: number;
  engine: string;
  cubic: number;
  horsepower: number;
  price: number;
  description: string;
}> = [
  {
    model: 'LaFerrari Aperta',
    year: 2016,
    speed: 350,
    toHundred: 3,
    engine: 'V12',
    cubic: 6262,
    horsepower: 963,
    price: 2170000,
    description: "Ferrari's unique core values have been raised to a whole new level in the car launched to mark the 70th anniversary of the foundation of the company.  Designed for Ferrari’s most passionate clients, the LaFerrari Aperta is the new limited-edition special series model, and just a few examples – all of them already accounted for - of this spider version of the acclaimed LaFerrari supercar will be built. The LaFerrari Aperta’s styling retains the essential characteristics of the coupé. It is a futuristic and absolutely extreme car that seamlessly marries form and function whilst still retaining clear links to classic Ferrari styling cues."
    },
    {
      model: 'F8 Spider',
      year: 2020,
      speed: 340,
      toHundred: 2.9,
      engine: 'V8 biturbo',
      cubic: 3902,
      horsepower: 720,
      price: 262000,
      description:"The F8 Spider’s greatest achievement is the fact that it unleashes its power instantaneously with zero turbo lag, whilst retaining this V8’s unique and very special soundtrack. To achieve their goal of making the car’s impressive performance easy to access and control, Ferrari’s engineers worked on integrating engine and aerodynamics performance with the latest iteration of the vehicle dynamics control systems. The F8 Spider’s cockpit retains the classic, driver-oriented look typical of Ferrari’s mid-rear-engined berlinettas. A concept that creates a symbiotic relationship between driver and car."
    },
  {
    model: 'Portofino',
    year: 2020,
    speed: 320,
    toHundred: 3.5,
    engine: 'V8 biturbo',
    cubic: 3855,
    horsepower: 600,
    price: 193400,
    description: "Ferrari has chosen a particularly evocative moniker for this exceptionally versatile drop-top car, referencing one of Italy’s most beautiful towns. Portofino is renowned for its charming tourist port and, over the years, has become internationally synonymous with elegance, sportiness and understated luxury. The launch colour of the new Ferrari has also been dedicated to this marvellous town: Rosso Portofino. A Ferrari designed to be driven every day that also effortlessly converts from an authentic ‘berlinetta’ coupé to a drop-top capable of delivering a unique Ferrari soundtrack and superb driving pleasure even in day-to-day situations. "
  },
  {
    model: 'SF90 Stradale',
    year: 2019,
    speed: 340,
    toHundred: 2.5,
    engine: 'V8 biturbo',
    cubic: 3990,
    horsepower: 1000,
    price: 423500,
    description:"The car’s name encapsulates the true significance of all that has been achieved in terms of performance. The reference to the 90th anniversary of the foundation of Scuderia Ferrari underscores the strong link that has always existed between Ferrari’s track and road cars. A brilliant encapsulation of the most advanced technologies developed in Maranello, the SF90 Stradale is also the perfect demonstration of how Ferrari immediately transitions the knowledge and skills it acquires in competition to its production cars. The SF90 Stradale is the first ever Ferrari to feature PHEV (Plug-in Hybrid Electric Vehicle) architecture which sees the internal combustion engine integrated with three electric motors, two of which are independent and located on the front axle, with the third at the rear between the engine and the gearbox."
  },
];

export default data;

module.exports = (isProd) => ({
    prefix: '',
    future: {
      removeDeprecatedGapUtilities: true,
      purgeLayersByDefault: true
    },
    purge: {
      enabled: isProd,
      content: ['**/*.html', '**/*.ts']
    },
    theme: {
      extend: {
        colors: {
          ferrari: {
            black: '#000000',
            yellow: '#FFF200',
            white: '#FFFFFF',
            red: '#CD212A',
            green: "#008C45"
          }
        }
      },
    }
});

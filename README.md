# Projet Angular - Site Ferrari
## By TALON ROMAIN and MORICEAU Axel.
## DAWIN A 2020

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.6. <br>
### ⚠️ For the best possbile experience it's recommended to run this project on a 1920*1080px screen since the site is not responsive ⚠️
<hr><br>

## In order to run this project you need:
- npm
- angular
>`npm install -g @angular/cli`<br>


## To run this project:
1. Clone the repo :
>`git clone https://gitlab.com/axel.moriceau/talon-moriceau-angular`  

2. Install the dependencies
>`npm install`<br>


3. Build the project
>`npm run build` <br>
> This step is needed since we use a custom build script.

4. Run the project
>`npm run start` <br>
> It will open a webpage at http:localhost:4200

### ⚠️ For the best possbile experience it's recommended to run this project on a 1920*1080px screen since the site is not responsive ⚠️
